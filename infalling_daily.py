#!/usr/bin/env python3
from pathlib import Path
import os
import datetime

SIMULATIONS = [
    #  (55, 'l', '05'),
    #  (55, 'l', '1'),
    #  (55, 'l', '2'),
    (55, 'm', '05'),
    (55, 'm', '1'),
    (55, 'm', '2'),
    (55, 'h', '05'),
    (55, 'h', '1'),
    (55, 'h', '2'),
    (56, 'h', '05'),
    (56, 'h', '1'),
    (56, 'h', '2'),
    #  (57, 'l', '05'),
    #  (57, 'l', '1'),
    #  (57, 'l', '2'),
    #  (57, 'm', '05'),
    #  (57, 'm', '1'),
    #  (57, 'm', '2'),
    (57, 'h', '05'),
    (57, 'h', '1'),
    (57, 'h', '2'),
]

DIRECTORIES = {
    f'{R}{n}_M{M}': f'/net/hyperion/scratch/saeed/Results/rhyme-runs/rhyme_runs/CGM/infalling_cold_clump/L{R}.00Hz/{R}{n}_M{M}/output'
    for (R, n, M) in SIMULATIONS
}

RSYNC_COMMAND = 'rsync -u infalling_cold_clump /net/galaxy-data/export/galaxy/shared/MUSE/noBackup/user/saeed/CGM-20200824/CGM'
RSYNC_WD = '/net/hyperion/scratch/saeed/Results/rhyme-runs/rhyme_runs/CGM'

IMAGE_COMMAND = './image_gen'
IMAGE_WD = '/net/hyperion/scratch/saeed/Results/rhyme-runs/rhyme_runs/CGM/infalling_cold_clump'

MOVIES_COMMAND = 'rm -r movies && ./movie_gen'
MOVIES_WD = '/net/hyperion/scratch/saeed/Results/rhyme-runs/rhyme_runs/CGM/infalling_cold_clump'

def is_reachable(directory):
    if Path(directory).exists():
        return True
    else:
        return False

def get_newest_file_name(directory, ext):
    dir_path = Path(directory)
    return max([f for f in dir_path.resolve().glob('**/*' + ext)], key=os.path.getctime)

def get_newest_file_time(directory):
    dir_path = Path(directory)
    return max([os.path.getctime(f) for f in dir_path.resolve().glob('**/*')])


def email_report(report, images=[]):
    msg = '\n'.join(report)
    attachments = ' '.join([f'-a {img}' for img in images])
    os.system(f'echo \"{msg}\" | mutt {attachments} -s \"Infalling - {datetime.datetime.now()}\" -- s.sarpas@gmail.com')


def main():
    healthy = True
    report = []

    report.append('Checking directories:')

    for ID, directory in DIRECTORIES.items():
        if is_reachable(directory):
            report.append(f'- {ID:8s}: Reachable')
        else:
            report.append(f'- {ID:8s}: Uneachable')
            healthy = False

    report.append('')

    if not healthy:
        email_report(report)
        return

    report.append('Backing up...')
    rsync_command = f'cd {RSYNC_WD} && {RSYNC_COMMAND}'

    stat = os.system(rsync_command)

    if stat == 0:
        report.append('- has been successfull!')
    else:
        report.append('- has failed!')


    now = datetime.datetime.now()
    dt_max = datetime.timedelta(hours=6)

    report.append('')


    report.append('Checking the newest snapshot:')
    for ID, directory in DIRECTORIES.items():
        file_time = datetime.datetime.fromtimestamp(get_newest_file_time(directory))
        dt = now - file_time
        report.append(f"- {ID:8s}: {dt} {'Carefull!' if dt > dt_max else ''}")


    report.append('')

    report.append('Generate images...')
    image_command = f'cd {IMAGE_WD} && {IMAGE_COMMAND}'

    stat = os.system(image_command)

    if stat == 0:
        report.append('- has been successfull!')
    else:
        report.append('- has failed!')


    report.append('')

    report.append('Generating movies...')
    movies_command = f'cd {MOVIES_WD} && {MOVIES_COMMAND}'

    stat = os.system(movies_command)

    if stat == 0:
        report.append('- has been successfull!')
    else:
        report.append('- has failed!')

    report.append('')

    images = []


    for ID, directory in DIRECTORIES.items():
        images.append(get_newest_file_name(f'{IMAGE_WD}/images/{ID}/col_rec', ext='.png'))


    email_report(report, images)


if __name__ == '__main__':
    main()
