#!/usr/bin/env bash

wd="/net/theia/scratch/saeed/development/scrips"
cd ${wd}

touch "reports/infalling_$(date +%F).txt"

./infalling_daily.py >> reports/infalling_$(date +%F).txt

echo "bash ./run_infalling_daily.sh" | at 6:00 AM tomorrow
